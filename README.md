# Spring Boot 2 hello world api in AWS Lambda
This project can be used as a starting point for building an api with Spring Boot and AWS Lambda. 
Uses CloudFormation, Maven and AWS cli.

## Deploying
1. Modify BUCKET in deploy.sh. 
BUCKET is the name of the s3 bucket that will be created, so it needs to be globally unique.
2. If you don't want to use your default AWS profile, remember to run `export AWS_PROFILE=<my-profile>`.
Many of the commands used by deploy.sh expect a region configuration, and it is not hard-coded in those commands,
so it is recommended to define it in ~/.aws/config
3. Run `./deploy.sh`. This will create a jar file with all the dependencies bundled in, 
create an s3 bucket if it doesn't exist, upload the jar in s3 and run cloudformation template 
defined in cf-template.yml to deploy the stack. Finally, a new Lambda version will be published and an alias
will be updated to point to the new version.
4. The template creates a lambda function, a function url, and the required permissions. After the stack is
created, you should be able to call the Spring Boot api at `<function url>/hello`. 
You can find the function url in Lambda settings in AWS console 
or with the describe-stacks command under outputs and HttpEndpoint. Notice that since we use
SnapStart, publishing a new function version takes a couple of minutes. So if you get an error when
you navigate to `<function url>/hello`, you might want to wait for a few minutes and try again.

## NOTE!
- Currently, deploy.sh doesn't do any clean up. The old Lambda versions should probably be cleaned up at some point.
- When we publish a new version, we should wait for the new version being ready before pointing the alias to it.
With the current approach there is a few minutes of downtime with each deployment
- In production, you probably would want to have a Cloudfront distribution in front of the Lambda function