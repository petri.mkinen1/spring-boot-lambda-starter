#!/bin/bash

FUNCTION_NAME="Hello"
ALIAS="DEV"
BUCKET="hellocodebucket-lambda"
JAR_FILE="helloworld-1.0-SNAPSHOT.jar"
S3KEY="$JAR_FILE"
REGION=$(aws configure get region)

echo "Using region $REGION"

mvn clean package

aws s3api create-bucket --bucket $BUCKET --create-bucket-configuration LocationConstraint="$REGION"

aws s3 cp target/"$JAR_FILE" s3://"$BUCKET"/"$S3KEY"

aws cloudformation deploy --template-file cf-template.yml \
--stack-name hello-stack \
--parameter-overrides \
BucketName="$BUCKET" \
FunctionName="$FUNCTION_NAME" \
Alias="$ALIAS" \
S3Key="$S3KEY" \
--capabilities CAPABILITY_NAMED_IAM

NEW_VERSION=$(aws lambda update-function-code --function-name "$FUNCTION_NAME" --s3-bucket "$BUCKET" --s3-key "$S3KEY" --publish --query "to_number(Version)")

echo "Updating alias $ALIAS to point to new version $NEW_VERSION"
aws lambda update-alias --function-name "$FUNCTION_NAME" --name "$ALIAS" --function-version "$NEW_VERSION"